# =============================================================================
# Advanced Topics in Machine Learning: Project on Class Imbalance/Active Learning
# Charilaos Bezdemiotis, mpezdemc@csd.auth.g, AM: 77, Spring Semester 2021
# Agelos Mourelis, amourelis@csd.auth.gr, AM: 61, Spring Semester 2021
# =============================================================================

import numpy as np
import torch
from sklearn.metrics import balanced_accuracy_score
from torch import nn, optim
from torch.utils.data import Dataset
import torch.nn.functional as F


class MusicGenreSubset(Dataset):
    def __init__(self, X_train, y_train):
        self.X_train = X_train
        self.y_train = y_train

    def __len__(self):
        return len(self.y_train)

    def __getitem__(self, idx):
        return [self.X_train[idx], self.y_train[idx]]


class MusicGenreEvalSubset(Dataset):
    def __init__(self, X_train):
        self.X_train = X_train

    def __len__(self):
        return len(self.X_train)

    def __getitem__(self, idx):
        return self.X_train[idx]

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(30, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)
        self.d1 = nn.Dropout(0.25)
        self.d2 = nn.Dropout(0.25)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, x):

        x = F.relu(self.fc1(x))
        x = self.d1(x)
        x = F.relu(self.fc2(x))
        x = self.d2(x)
        x = self.fc3(x)
        return x

class PytorchNeuralNetwork:

    def __init__(self):
        self.net = Net()
        self.n_classes = 10
        self.batch_size = 200
        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.net.parameters(), lr=0.0005)

    def predict_proba(self, X):

        loader = self.create_eval_loader(X)

        decision = np.zeros((len(X), 10))
        self.net.eval()
        with torch.no_grad():
            count = 0
            for data in loader:
                images = data
                outputs = self.net(images.float())
                for output in outputs:
                    decision[count] = torch.nn.functional.softmax(output,dim=0).numpy()
                    count += 1

        return decision


    def partial_fit(self, X, y, classes=None):
        self.train_to_samples(X, y)

    def train_to_samples(self, X_train, y_train):

        trainLoader = self.create_loader(X_train, y_train)
        self.trainOnLoader(trainLoader)

    def get_acc_on_loader(self, X_test, y_test):
        testLoader = self.create_loader(X_test, y_test)
        return self.evatTestSet(testLoader)


    def evatTestSet( self, testLoader):
        total = 0
        correct = 0
        all_preds = torch.tensor([])
        all_labels = torch.tensor([])
        self.net.eval()
        with torch.no_grad():
            for data in testLoader:
                images, labels = data[0], data[1]
                outputs = self.net(images.float())
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                all_preds = torch.cat((all_preds, predicted), dim=0)
                all_labels = torch.cat((all_labels, labels), dim=0)
                correct += (predicted == labels).sum().item()
        y_true = all_labels.cpu().numpy()
        y_pred = all_preds.cpu().numpy()
        print(balanced_accuracy_score(y_true, y_pred))
        return balanced_accuracy_score(y_true, y_pred)
        #print(f1_score(y_true, y_pred, average='macro'))
        #return f1_score(y_true, y_pred, average='macro')

    def trainOnLoader( self, loader):
        for epoch in range(50):  # loop over the dataset multiple times

            running_loss = 0.0
            for i, data in enumerate(loader, 0):
                self.net.train()
                # get the inputs; data is a list of [inputs, labels]
                inputs, labels = data

                # zero the parameter gradients
                self.optimizer.zero_grad()

                # forward + backward + optimize
                outputs = self.net(inputs.float())
                loss = self.criterion(outputs, labels)
                loss.backward()

                self.optimizer.step()

                # print statistics
                running_loss += loss.item()
                #if i % 2000 == 1999:  # print every 2000 mini-batches
                #    evatTestSet(net, testLoader

    def create_loader(self, x, y):
        dataSubset = MusicGenreSubset(x, y)
        loader = torch.utils.data.DataLoader(dataSubset, batch_size=self.batch_size, shuffle=True)
        return loader

    def create_eval_loader(self, x):
        dataSubset = MusicGenreEvalSubset(x)
        loader = torch.utils.data.DataLoader(dataSubset, batch_size=self.batch_size, shuffle=True)
        return loader


# dataset = MusicGenreDataset('../Datasets/msd_genre_dataset.txt')
# dataset.read_csv()
# X_train, X_test, y_train, y_test = dataset.get_train_test_split()
#
# neu = PytorchNeuralNetwork()
# neu.train_to_samples(X_train,y_train)
# neu.get_acc_on_loader(X_test,y_test)
# neu.predict_proba(X_test)
#
# ml = MLPClassifier(max_iter=10)
# ml.fit(X_train,y_train)
# y = ml.predict_proba(X_test)


