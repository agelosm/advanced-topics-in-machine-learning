import pickle

import matplotlib
import matplotlib.pyplot as plt

al_names = ['Uncertainty_Oversample_False', 'Uncertainty_False']
al_titles = ['Uncertainty with oversampling', 'Uncertainty']

al_names_2 = ['Uncertainty_Oversample_False', 'Uncertainty_False']
al_titles_2 = ['Uncertainty wtih oversampling', 'Uncertainty']


fig, (ax1, ax2) = plt.subplots(1, 2, sharey=False, sharex=True)

fig.text(0.5, 0.04, "Percentage(%) of the training set", ha='center', fontsize=18)

font = {'family': 'normal',
        'size': 18}

matplotlib.rc('font', **font)


xax = []

for i in range(36):
    xax.append(((1700+1000*i)/35500)*100)

ax1.set_xlim(5,100)
ax2.set_xlim(5,100)

length = 0
for name, title in zip(al_names, al_titles):
    with open( '../' + name + '.pickle', 'rb') as fp:
        res = pickle.load(fp)
        length = res[0]
        ax1.plot(xax,res, label=title)

full_acc = 0.6922931854601156



ax1.hlines(full_acc, 5, 100, label='Whole dataset', colors='maroon')
ax1.grid()
ax1.set(title='Balanced accuracy')


length = 0
for name, title in zip(al_names_2, al_titles_2):
    with open( '../' + name + '_F1.pickle', 'rb') as fp:
        res = pickle.load(fp)
        #length = res[0]
        ax2.plot(xax, res, label=title)

full_f1 = 0.6952931854601156
ax2.grid()
ax2.hlines(full_f1, 5, 100, label='Whole dataset',  colors='maroon')
ax2.set(title='F1')

ax1.set_xticks([5,20,40,80,60,100])

ax1.legend(loc='lower right')
ax2.legend()
plt.show()


1700