import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def bar_plot_metrics(my_dict_acc,my_dict_f1, vars ):

    y_pos = np.arange(len(vars))
    width = 0.35
    accuracy_vals = []
    f1_vals = []
    for var in vars:
        accuracy_vals.append(my_dict_acc[var])
        f1_vals.append(my_dict_f1[var])

    fig,ax = plt.subplots()
    rectsacc = ax.bar(y_pos-width/2,accuracy_vals,width,label = 'Balanced Accuracy')
    rectsf1 = ax.bar(y_pos + width/2, f1_vals, width, label='F1 (weighted)')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Scores')
    ax.set_xticks(y_pos)
    ax.set_xticklabels(vars)
    ax.legend()

    # ax.bar_label(rectsacc, padding=3)
    # ax.bar_label(rectsf1, padding=3)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    fig.tight_layout()
    return 1

def plot_distribution(df,kind,title=None):
    plt.figure()
    if title != None:
        plt.title(title)
    df.plot(kind=kind)
    plt.minorticks_on()
    plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
    plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    return 1

def make_binarization(df,key,plot = False):
    genre_list = []
    cnt = 0
    for item in df.iloc[:, 0]:
        if item == key:
            genre_list.append(key)
        else:
            genre_list.append('not' + key)
    genre_df = pd.DataFrame(genre_list)

    # PLOT class distribution after binarization
    if plot:
        label_count = genre_df.value_counts()
        plot_distribution(label_count, kind='pie')
        plot_distribution(label_count, kind='pie')

    return genre_df

def decode(y,genre_df):
    labels_set = set(y)
    key_to_original_label = dict()
    i = 0
    continue_looping = True
    while continue_looping:
        if y[i] in labels_set:
            key_to_original_label[y[i]] = genre_df[i]
            labels_set.remove(y[i])
        if labels_set == set():
            continue_looping = False
        i += 1

    columns = []
    for i in range(len(set(y))):
        columns.append(key_to_original_label[i])

    return key_to_original_label,columns

def build_distribution_over(y_train,key='iso'):
    ret = pd.DataFrame(y_train).value_counts()
    ascending_ord = []
    for x in ret.sort_values():
        ascending_ord.append(x)
    key_to_val = dict()
    maximum_freq = max(ret)

    if key == 'iso':
        for key, val in ret.items():
            key_to_val[int(str(key)[1])] = maximum_freq
    elif key == 'over1':
        for key, val in ret.items():
            if val < ascending_ord[-2]:
                num = ascending_ord[-2]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'over2':
        for key, val in ret.items():
            if val < ascending_ord[-3]:
                num = ascending_ord[-3]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'over3':
        for key, val in ret.items():
            if val * 8 > maximum_freq:
                num = val
            elif val * 16 > maximum_freq:
                num = val * 8
            else:
                num = val * 50
            key_to_val[int(str(key)[1])] = num
    else:
        return -1

    return key_to_val


def build_distribution_under(y_train,key='iso'):
    ret = pd.DataFrame(y_train).value_counts()
    ascending_ord = []
    for x in ret.sort_values():
        ascending_ord.append(x)

    key_to_val = dict()
    if key == 'iso':
        for key, val in ret.items():
            key_to_val[int(str(key)[1])] = ascending_ord[0]
    elif key == 'under1':
        for key, val in ret.items():
            if val >=  ascending_ord[-2]:
                num = ascending_ord[-2]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'under2':
        for key, val in ret.items():
            if val >= ascending_ord[-3]:
                num = ascending_ord[-3]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'under3':
        for key, val in ret.items():
            if val >=  ascending_ord[-4]:
                num = ascending_ord[-4]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    else:
        return -1
    return key_to_val