# =============================================================================
# Advanced Topics in Machine Learning: Project on Class Imbalance/Active Learning
# Charilaos Bezdemiotis, mpezdemc@csd.auth.g, AM: 77, Spring Semester 2021
# Agelos Mourelis, amourelis@csd.auth.gr, AM: 61, Spring Semester 2021
# =============================================================================
import os
import pickle
import time
from enum import Enum

from imblearn.over_sampling import SMOTE
from sklearn.model_selection import StratifiedShuffleSplit

import common_funcs
import matplotlib
from alipy.query_strategy import QueryInstanceUncertainty, QueryInstanceGraphDensity, QueryInstanceQBC, \
    QueryInstanceBMDR, QueryInstanceLAL
from matplotlib import pyplot as plt
from sklearn.metrics import f1_score
from sklearn.neural_network import MLPClassifier
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from pytorch import PytorchNeuralNetwork

from MusicGenreDataset import MusicGenreDataset


class Metric(Enum):
    WEIGHTED_F1 = 0
    BALANCED_ACCURACY = 1


class ActiveLearner:
    """A base class that represents an abstract method of Active Learning

       Attributes:
           X_train:   Training features
           X_test:   Test features
           y_train: Train labels
           y_test: Test labels
           labeled_indices: Indices on X_train, of the samples that are initially labeled
           unlabeled_indices: Symmetrically, indices on X_train, of the samples that are initially labeled
           batch_size: How many indices does the algorithm selects in one step
    """

    def __init__(self, model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size):
        self.labeled_indices = labeled_indices
        self.unlabeled_indices = unlabeled_indices
        self.model = model
        self.batch_size = batch_size
        self.accuracies = []
        self.x_train = X_train
        self.x_test = X_test
        self.y_test = y_test
        self.y_train = y_train
        self.y_empty = [-1] * len(X_train)

        # The class who overloads, must initialize it's own uncertainStrategy
        self.uncertainStrategy = None
        self.title = None
        self.model.partial_fit(X=self.x_train[self.labeled_indices, :], y=self.y_train[self.labeled_indices],
                               classes=range(10))

        for i in labeled_indices:
            self.y_empty[i] = self.y_train[i]

    def select(self):
        return self.uncertainStrategy.select(self.labeled_indices, self.unlabeled_indices,
                                             model=self.model, batch_size=self.batch_size)

    def apply_next_selection(self):
        select_indices = self.select()

        if self.fit_in_batches:
            self.model.partial_fit(X=self.x_train[select_indices, :], y=self.y_train[select_indices],
                                   classes=range(10))

        # Subtract the indices of the samples that we are about to select from the unlabeled ones
        self.unlabeled_indices = np.setdiff1d(self.unlabeled_indices, select_indices, assume_unique=True)

        # Concatenate the indices of the samples that we are about to select with the labeled ones
        self.labeled_indices = np.concatenate((self.labeled_indices, select_indices), axis=0)

    def append_accuracy(self, acc):
        self.accuracies.append(acc)

    def save_accuracies(self):
        with open(self.title + '.pickle', 'wb') as handle:
            pickle.dump(self.accuracies, handle)

    def calculate_accuracies(self):

        # Loop until all indices are labeled
        while len(self.unlabeled_indices) > 0:

            print(str(len(self.labeled_indices)) + '/' + str(len(self.x_train)))

            # In case we end up with less unlabeled indices than our batch size
            if len(self.unlabeled_indices) < self.batch_size:
                self.batch_size = len(self.unlabeled_indices)

            self.apply_next_selection()

            # Fit the model on the indices that include the ones that we just selected
            if not self.fit_in_batches:
                self.model.partial_fit(X=self.x_train[self.labeled_indices, :], y=self.y_train[self.labeled_indices],
                                       classes=range(10))

            # pred = self.model.predict(self.x_test)
            # acc = f1_score(self.y_test, pred, average='weighted')
            acc = self.model.get_acc_on_loader(self.x_test, self.y_test)
            self.append_accuracy(acc)


# Query by uncertainty
class UncertaintyLearner(ActiveLearner):

    def __init__(self, model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size, fit_in_batches=False):
        super().__init__(model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size)
        self.fit_in_batches = fit_in_batches
        self.uncertainStrategy = QueryInstanceUncertainty(self.x_train, self.y_empty)
        self.title = 'Uncertainty_' + str(fit_in_batches)


class UncertaintyLearnerWithOversample(ActiveLearner):

    def __init__(self, model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size, fit_in_batches=False):
        super().__init__(model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size)
        self.fit_in_batches = fit_in_batches
        self.uncertainStrategy = QueryInstanceUncertainty(self.x_train, self.y_empty)
        self.title = 'Uncertainty_Oversample_' + str(fit_in_batches)

    def apply_next_selection(self):
        select_indices = self.select()

        if self.fit_in_batches:
            key_to_val = common_funcs.build_distribution_over(self.y_train[select_indices], 'over1')
            sampler = SMOTE(sampling_strategy=key_to_val, random_state=None, k_neighbors=5, n_jobs=None)
            X_train_over, y_train_over = sampler.fit_resample(self.x_train[select_indices, :],
                                                              self.y_train[select_indices])
            self.model.partial_fit(X_train_over, y_train_over, classes=range(10))


        # Subtract the indices of the samples that we are about to select from the unlabeled ones
        self.unlabeled_indices = np.setdiff1d(self.unlabeled_indices, select_indices, assume_unique=True)

        # Concatenate the indices of the samples that we are about to select with the labeled ones
        self.labeled_indices = np.concatenate((self.labeled_indices, select_indices), axis=0)

    def calculate_accuracies(self):

        # Loop until all indices are labeled
        while len(self.unlabeled_indices) > 0:

            print(str(len(self.labeled_indices)) + '/' + str(len(self.x_train)))

            # In case we end up with less unlabeled indices than our batch size
            if len(self.unlabeled_indices) < self.batch_size:
                self.batch_size = len(self.unlabeled_indices)

            self.apply_next_selection()

            if not self.fit_in_batches:
                key_to_val = common_funcs.build_distribution_over(self.y_train[self.labeled_indices], 'over1')
                sampler = SMOTE(sampling_strategy=key_to_val, random_state=None, k_neighbors=5, n_jobs=None)
                X_train_over, y_train_over = sampler.fit_resample(self.x_train[self.labeled_indices, :],
                                                                  self.y_train[self.labeled_indices])
                self.model.partial_fit(X=X_train_over, y=y_train_over, classes=range(10))

            # Fit the model on the indices that include the ones that we just selected


            # pred = self.model.predict(self.x_test)
            # acc = f1_score(self.y_test, pred, average='weighted')
            acc = self.model.get_acc_on_loader(self.x_test, self.y_test)
            self.append_accuracy(acc)


# Query by committee
class CommitteeLearner(ActiveLearner):

    def __init__(self, model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size):
        super().__init__(model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size)

        self.uncertainStrategy = QueryInstanceQBC(self.x_train, self.y_empty)
        self.title = 'Committee_Oversample'


# BMDR
class BMDRLearner(ActiveLearner):

    def __init__(self, model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size):
        super().__init__(model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size)

        self.uncertainStrategy = QueryInstanceLAL(self.x_train, self.y_empty)
        self.title = 'BMDR'


# Graph density
class DensityLearner(ActiveLearner):

    def __init__(self, model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size):
        super().__init__(model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size)

        self.uncertainStrategy = QueryInstanceGraphDensity(self.x_train, self.y_train,
                                                           np.concatenate(
                                                               (self.unlabeled_indices, self.labeled_indices), axis=0))

        self.title = 'Density'


# An algorithm that just select random indices
class RandomLearner(ActiveLearner):

    def __init__(self, model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size, fit_in_batches=False):
        super().__init__(model, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, batch_size)
        self.fit_in_batches = fit_in_batches
        self.title = 'Random_' + str(fit_in_batches)

    def select(self):
        return np.random.choice(self.unlabeled_indices, self.batch_size, replace=False)


class ActiveLearningExperiment:

    def __init__(self, metric=Metric.WEIGHTED_F1, initial_labeled=100, batch_size=1000,
                 classifier=MLPClassifier(random_state=1, max_iter=50)):
        self.metric = metric
        self.how_many_to_start_with = initial_labeled
        self.batch_size = batch_size
        self.classifier = classifier
        self.dataset = MusicGenreDataset('../Datasets/msd_genre_dataset.txt')
        self.dataset.read_csv()

    def plot_metrics_from_pickle_files(self):
        accuracy_list = []

        # Open the pcikle files, plot the saved data
        for filename in os.listdir('.'):
            if filename.endswith(".pickle"):
                with open(filename, 'rb') as handle:
                    accs = pickle.load(handle)
                    accuracy_list.append(accs)
                    plt.plot(accs, label=filename.split('.')[0])

        font = {'family': 'normal',
                'size': 18}

        matplotlib.rc('font', **font)

        # Plot a constant line of the full train set metric
        plt.hlines(self.full_model_acc, 0, len(accuracy_list[0]), label='Whole dataset', colors='maroon')
        plt.xlabel('Train percentage', fontsize=18)
        plt.ylabel(str(self.metric), fontsize=18)

        # Construct 4 x axis ticks, with the percentage of the train set that was labeled at the time
        plt.grid(True, axis='x')
        plt.legend()
        ticks = list(range(0, len(accuracy_list[0]), 4))
        tick_vals = [((x * self.batch_size + self.how_many_to_start_with) / self.dataset.train_len) * 100 for x in
                     ticks]
        tick_vals_2f = []
        for acc in tick_vals:
            tick_vals_2f.append(format(acc, '.2f'))
        plt.xticks(ticks, tick_vals_2f, fontsize=18)
        plt.yticks(fontsize=18)
        plt.show()

    def run_and_save_metrics(self):

        X_train, X_test, y_train, y_test = self.dataset.get_train_test_split()

        # Create a random permutation, that will ne used for some random samples to begin as labeled
        permutation = np.random.permutation(len(X_train))

        sss = StratifiedShuffleSplit(n_splits=20, random_state=0)
        split = sss.split(X_train, y_train).__next__()

        # Split the permutation for labeled/unlabeled
        #labeled_indices = np.array(permutation[0:self.how_many_to_start_with])
        #unlabeled_indices = np.array(permutation[self.how_many_to_start_with:len(X_train)])

        labeled_indices = split[1]
        unlabeled_indices = np.setdiff1d(range(1,len(X_train)), split[1], assume_unique=True)

        # Make a prediction on the full training set, to hold as a reference point
        self.classifier.partial_fit(X_train, y_train)
        self.full_model_acc = self.classifier.get_acc_on_loader(X_test, y_test)
        # pred_full = self.classifier.predict(X_test)
        # self.full_model_acc = f1_score(y_test, pred_full, average='weighted')
        print('Baseline F1 score on full training set: ' + str(self.full_model_acc))

        # Initialize the 3 active learning algorithms, plus one that generates random indices to select
        active_learners = [
            UncertaintyLearner(PytorchNeuralNetwork(), X_train, X_test, y_train, y_test, labeled_indices,unlabeled_indices, self.batch_size),
            UncertaintyLearnerWithOversample(self.classifier, X_train, X_test, y_train, y_test, labeled_indices,
                                             unlabeled_indices, self.batch_size),
            CommitteeLearner(self.classifier, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, self.batch_size),
            DensityLearner(self.classifier, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, self.batch_size),
            BMDRLearner(self.classifier, X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices, self.batch_size),
            RandomLearner(PytorchNeuralNetwork(), X_train, X_test, y_train, y_test, labeled_indices, unlabeled_indices,
                          self.batch_size)]


        # Gradually increment the labeled indices for each algorithm, and collect the results
        for active_learner in active_learners:
            print('Starting ' + active_learner.title + ' at ' + str(time.ctime()))
            active_learner.calculate_accuracies()
            print('Finished ' + active_learner.title + ' at ' + str(time.ctime()))
            # Save collected accuracies in pickle format
            active_learner.save_accuracies()


def main():

    al = ActiveLearningExperiment(metric=Metric.BALANCED_ACCURACY, classifier=PytorchNeuralNetwork())
    al.run_and_save_metrics()
    al.plot_metrics_from_pickle_files()


if __name__ == "__main__":
    main()
