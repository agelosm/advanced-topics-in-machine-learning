# =============================================================================
# Advanced Topics in Machine Learning: Project on Class Imbalance/Active Learning
# Charilaos Bezdemiotis, mpezdemc@csd.auth.g, AM: 77, Spring Semester 2021
# Agelos Mourelis, amourelis@csd.auth.gr, AM: 61, Spring Semester 2021
# =============================================================================
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def bar_plot_metrics(my_dict_acc,my_dict_f1, vars ):
    y_pos = np.arange(len(vars))
    width = 0.35
    accuracy_vals = []
    f1_vals = []
    for var in vars:
        accuracy_vals.append(my_dict_acc[var])
        f1_vals.append(my_dict_f1[var])

    fig,ax = plt.subplots()
    rectsacc = ax.bar(y_pos-width/2,accuracy_vals,width,label = 'Balanced Accuracy')
    rectsf1 = ax.bar(y_pos + width/2, f1_vals, width, label='F1 (weighted)')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Scores')
    ax.set_xticks(y_pos)
    ax.set_xticklabels(vars)
    ax.legend()

    # ax.bar_label(rectsacc, padding=3)
    # ax.bar_label(rectsf1, padding=3)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    fig.tight_layout()
    plt.show()
    return 1


# Builds an oversampling distribution, depending on the input key name:
# 'iso', 'var1', 'var2', 'var3'
def build_distribution_over(y_train,key='iso'):
    ret = pd.DataFrame(y_train).value_counts()
    ascending_ord = []
    for x in ret.sort_values():
        ascending_ord.append(x)
    key_to_val = dict()
    maximum_freq = max(ret)

    if key == 'iso':
        for key, val in ret.items():
            key_to_val[int(str(key)[1])] = maximum_freq
    elif key == 'var1':
        for key, val in ret.items():
            if val < ascending_ord[-2]:
                num = ascending_ord[-2]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'var2':
        for key, val in ret.items():
            if val < ascending_ord[-3]:
                num = ascending_ord[-3]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'var3':
        for key, val in ret.items():
            if val * 8 > maximum_freq:
                num = val
            elif val * 16 > maximum_freq:
                num = val * 8
            else:
                num = val * 50
            key_to_val[int(str(key)[1])] = num
    else:
        return -1

    return key_to_val


# Builds an undersampling distribution, depending on the input key name:
# 'iso', 'var1', 'var2', 'var3'
def build_distribution_under(y_train,key='iso'):
    ret = pd.DataFrame(y_train).value_counts()
    ascending_ord = []
    for x in ret.sort_values():
        ascending_ord.append(x)

    key_to_val = dict()
    if key == 'iso':
        for key, val in ret.items():
            key_to_val[int(str(key)[1])] = ascending_ord[0]
    elif key == 'var1':
        for key, val in ret.items():
            if val >=  ascending_ord[-2]:
                num = ascending_ord[-2]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'var2':
        for key, val in ret.items():
            if val >= ascending_ord[-3]:
                num = ascending_ord[-3]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    elif key == 'var3':
        for key, val in ret.items():
            if val >=  ascending_ord[-4]:
                num = ascending_ord[-4]
            else:
                num = val
            key_to_val[int(str(key)[1])] = num
    else:
        return -1
    return key_to_val
