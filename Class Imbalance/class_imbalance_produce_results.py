# =============================================================================
# Advanced Topics in Machine Learning: Project on Class Imbalance/Active Learning
# Charilaos Bezdemiotis, mpezdemc@csd.auth.g, AM: 77, Spring Semester 2021
# Agelos Mourelis, amourelis@csd.auth.gr, AM: 61, Spring Semester 2021
# =============================================================================
from enum import Enum

import pandas as pd
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import f1_score, precision_score, recall_score, balanced_accuracy_score
import class_imbalance_common_funcs as common_funcs
from MusicGenreDataset import MusicGenreDataset
from imblearn.over_sampling import BorderlineSMOTE, SMOTE
from imblearn.under_sampling import NearMiss
import matplotlib.pyplot as plt
from confusion_matrix_pretty_print import plot_confusion_matrix_from_data


class OverSamplingType(Enum):
    SMOTE = 1
    BORDERLINE_SMOTE = 2


def print_scores(y_true, y_predicted):
    print('balanced accuracy:', balanced_accuracy_score(y_true, y_predicted))
    print('precision', precision_score(y_true, y_predicted, average="macro"))
    print('recall', recall_score(y_true, y_predicted, average="macro"))
    print('f1:', f1_score(y_true, y_predicted, average="macro"))


class ClassImbalanceSampler:
    """A base class that represents an abstract method of a class imbalance sampling method

       Attributes:
           X_train:     Training features
           y_train:     Train labels
    """

    def __init__(self, X_train, y_train):
        self.variations = ['iso', 'var1', 'var2', 'var3']
        self.var_to_dict = dict()
        self.X_train = X_train
        self.y_train = y_train
        self.label_count_sampled = []
        for var in self.variations:
            key_to_val = self.build_distribution(var)
            self.var_to_dict[var] = key_to_val

    def plot_distributions(self):

        fig = plt.figure()
        gs = fig.add_gridspec(nrows=1, ncols=5)
        axs = gs.subplots(sharex=True, sharey=True)
        fig.suptitle(self.prefix + ' Sampling Variations')

        # First subplot will be the initial distribution, no sampling
        label_count_no_sampling = pd.DataFrame(self.y_train).value_counts()
        label_count_no_sampling.plot(kind='bar', ax=axs[0])
        axs[0].set_title('No sampling')

        # Hide x labels and tick labels for all but bottom plot.
        for ax in axs:
            x_axis = ax.axes.get_xaxis()
            x_axis.set_visible(False)
            ax.label_outer()

        # Add all other distributions to sub plots
        for cnt, [var, _] in enumerate(self.var_to_dict.items()):
            label_count_oversampled = self.label_count_sampled[cnt]
            axs[cnt + 1].set_title(var)
            label_count_oversampled.plot(kind='bar', ax=axs[cnt + 1])

        plt.show()


class ClassImbalanceOverSampler(ClassImbalanceSampler):
    """A base class that represents a class imbalance over-sampling method

       Attributes:
           X_train:                  Training features
           y_train:                  Train labels
           oversampling_type:        Smote or Borderline Smote
    """
    def __init__(self, X_train, y_train, oversampling_type=OverSamplingType.BORDERLINE_SMOTE):
        super().__init__(X_train, y_train)
        self.prefix = 'Over'

        if oversampling_type == OverSamplingType.BORDERLINE_SMOTE:
            self.oversampling_func = BorderlineSMOTE
            self.sample_prefix = 'borderlineSMOTE_'
        else:
            self.oversampling_func = SMOTE
            self.sample_prefix = 'SMOTE_'

    def build_distribution(self, var):
        return common_funcs.build_distribution_over(self.y_train, var)

    # Fill an input dictionary with the generated over-samplers
    def build_samplers(self, samplers):
        for cnt, [var, my_dict] in enumerate(self.var_to_dict.items()):
            samplers[self.sample_prefix + var] = self.oversampling_func(
                sampling_strategy=my_dict, random_state=None, k_neighbors=5, n_jobs=None)
            _, y_sampled = samplers[self.sample_prefix + var].fit_resample(self.X_train, self.y_train)
            self.label_count_sampled.append(pd.DataFrame(y_sampled).value_counts())


class ClassImbalanceUnderSampler(ClassImbalanceSampler):
    """A base class that represents a class imbalance under-sampling method, implementing NearMiss

       Attributes:
           X_train:                  Training features
           y_train:                  Train labels
    """
    def __init__(self, X_train, y_train):
        super().__init__(X_train, y_train)
        self.prefix = 'Under'

    def build_distribution(self, var):
        return common_funcs.build_distribution_under(self.y_train, var)

    # Fill an input dictionary with the generated under-samplers
    def build_samplers(self, samplers):
        for var, my_dict in self.var_to_dict.items():
            samplers['NearMiss_' + var] = NearMiss(
                sampling_strategy=my_dict, version=2  # size_ngh=None, # n_neighbors=3, # ver3_samp_ngh=None
            )
            _, y_sampled = samplers['NearMiss_' + var].fit_resample(self.X_train, self.y_train)
            self.label_count_sampled.append(pd.DataFrame(y_sampled).value_counts())


class SamplerEvaluator:
    """ Given an iterable container of class imbalance samplers, it evaluates them

       Attributes:
           dataset:                  The input dataset, already split in test & train
           print_scores_flag:        Print metrics in console or not
           show_plots:               Output bar plots of the sampling methods, and confusion matrices, or not
           classifier:               The underlying classifier, doesn't have to be fitted
    """
    def __init__(self, dataset, print_scores_flag=True, show_plots=False,show_bar_plots = True,
                 classifier=MLPClassifier(random_state=1, max_iter=200)):

        self.classifier = classifier
        self.dataset = dataset
        self.print_scores_flag = print_scores_flag
        self.show_plots = show_plots
        self.show_bar_plots = show_bar_plots

    def evaluate_samplers(self, samplers,key):
        best_acc = 0
        var_to_acc = {}
        var_to_f1 = {}
        best_model = None
        for my_str, sampler in samplers.items():
            if sampler == None:
                X_sampled, y_sampled = self.dataset.X_train, self.dataset.y_train
            else:
                X_sampled, y_sampled = sampler.fit_resample(self.dataset.X_train, self.dataset.y_train)

            self.classifier.fit(X_sampled, y_sampled)
            y_pred = self.classifier.predict(self.dataset.X_test)

            accuracy = balanced_accuracy_score(self.dataset.y_test, y_pred)
            f1w = f1_score(self.dataset.y_test, y_pred, average="weighted")
            var_to_acc[my_str.split('_')[-1]] = float(accuracy)
            var_to_f1[my_str.split('_')[-1]] = float(f1w)
            if accuracy > best_acc:
                best_acc = accuracy
                best_model = self.classifier

            if self.show_plots:
                test_labels = self.dataset.encoder.inverse_transform(self.dataset.y_test)
                pred_labels = self.dataset.encoder.inverse_transform(y_pred)
                plot_confusion_matrix_from_data(test_labels, pred_labels, columns=self.dataset.columns, title_str=str)

            if self.print_scores_flag:
                print(my_str + " -->")
                print_scores(self.dataset.y_test, y_pred)
                print('----------------------')

        if self.show_bar_plots:
            keys = [key+str(i) for i in range(1,4)]
            common_funcs.bar_plot_metrics(var_to_acc, var_to_f1, ['None'] + keys)

        return best_model

def __main__():
    dataset = MusicGenreDataset('../Datasets/msd_genre_dataset.txt')
    dataset.read_csv()
    X_train, X_test, y_train, y_test = dataset.get_train_test_split()

    samplers = {'None': None}
    overSampler = ClassImbalanceOverSampler(X_train, y_train,
                                            oversampling_type=OverSamplingType.BORDERLINE_SMOTE)
    overSampler.build_samplers(samplers)
    overSampler.plot_distributions()

    underSampler = ClassImbalanceUnderSampler(X_train, y_train)
    underSampler.build_samplers(samplers)
    underSampler.plot_distributions()

    evaluator = SamplerEvaluator(dataset, classifier=DecisionTreeClassifier())

    bestSmote = evaluator.evaluate_samplers(samplers,key='var')
    # bestNearMiss =  evaluator.evaluate_samplers(nearmiss_samplers)


if __name__ == '__main__':
    __main__()
