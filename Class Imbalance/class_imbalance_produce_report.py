import pandas as pd
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import LabelEncoder
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score,f1_score,precision_score,recall_score,balanced_accuracy_score,plot_confusion_matrix,confusion_matrix,roc_auc_score,roc_curve
from confusion_matrix_pretty_print import pretty_plot_confusion_matrix,plot_confusion_matrix_from_data
import common_funcs
from sklearn.model_selection import train_test_split
from collections import Counter

from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import BorderlineSMOTE
from imblearn.under_sampling import NearMiss
import matplotlib.pyplot as plt

df = pd.read_csv('../Datasets/msd_genre_dataset.txt')

make_binary = False
# PLOT class distribution
label_count = df.iloc[:, 0].value_counts()
# common_funcs.plot_distribution(label_count,kind='bar',title ='Origin Dataset')
common_funcs.plot_distribution(label_count,kind='pie',title ='Origin Dataset')

# do we want to binarize this example?
if make_binary:
    # make dataset binary
    genre_df = common_funcs.make_binarization(df,key = 'folk')
else:
    genre_df = df.iloc[:, 0]

# drop columns that do not provide knowledge
df = df.drop(labels='genre', axis=1)
df = df.drop(labels='track_id', axis=1)
df = df.drop(labels='artist_name', axis=1)
df = df.drop(labels='title', axis=1)

# encode the genre from stings to integers
encoder = LabelEncoder()
y = encoder.fit_transform(genre_df)
key_to_original_label,columns = common_funcs.decode(y,genre_df)#this is a workaround because encoder.decode does not work!

# normalize the data
scaler = StandardScaler()
X = scaler.fit_transform(np.array(df.iloc[:, :], dtype=float))
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

# plot training  dataset
common_funcs.plot_distribution(pd.DataFrame(encoder.inverse_transform(y_train)).value_counts(),kind='pie',title ='Train Dataset' )
# common_funcs.plot_distribution(pd.DataFrame(encoder.inverse_transform(y_train)).value_counts(),kind='bar',title ='Train Dataset' )

# plot test dataset
common_funcs.plot_distribution(pd.DataFrame(encoder.inverse_transform(y_test)).value_counts(),kind='pie',title ='Test Dataset' )
# common_funcs.plot_distribution(pd.DataFrame(encoder.inverse_transform(y_test)).value_counts(),kind='bar',title ='Test Dataset' )

# -----------------------------OVER SAMPLING------------------------------------------------
over_variations = ['iso','over1','over2','over3']
var_to_dict_over = dict()
for var in over_variations:
    key_to_val = common_funcs.build_distribution_over(y_train,var)
    var_to_dict_over[var] = key_to_val

#OVER sampling
fig = plt.figure()
gs = fig.add_gridspec(nrows=1,ncols=5)
axs = gs.subplots(sharex=True, sharey=True)
# fig.suptitle('Over Sampling Variations')

label_count_no_sampling = pd.DataFrame(y_train).value_counts()
label_count_no_sampling.plot(kind='bar',ax = axs[0])
axs[0].set_title('No sampling')

# Hide x labels and tick labels for all but bottom plot.
for ax in axs:
    x_axis = ax.axes.get_xaxis()
    x_axis.set_visible(False)
    ax.label_outer()
cnt = 1

samplers = {'None':None}
for var,my_dict in var_to_dict_over.items():
    samplers['SMOTE_'+var] = SMOTE(
                                    sampling_strategy=my_dict,
                                    random_state=None,
                                    k_neighbors=5,
                                    n_jobs=None
                                    )
    samplers['borderlineSMOTE_' + var] = BorderlineSMOTE(
                                                        sampling_strategy=my_dict,
                                                        random_state=None,
                                                        k_neighbors=5,
                                                        n_jobs=None
                                                         )
    # this is inefficient but its ok... we can live with it:
    X_sampled, y_sampled = samplers['borderlineSMOTE_' + var].fit_resample(X_train, y_train)
    label_count_oversampled = pd.DataFrame(y_sampled).value_counts()
    axs[cnt].set_title(var)
    label_count_oversampled.plot(kind='bar', ax=axs[cnt])
    cnt += 1

# -----------------------------UNDER SAMPLING------------------------------------------------
under_variations = ['iso','under1','under2','under3']
var_to_dict_under = dict()
for var in under_variations:
    key_to_val = common_funcs.build_distribution_under(y_train,var)
    var_to_dict_under[var] = key_to_val

#OVER sampling
fig = plt.figure()
gs = fig.add_gridspec(nrows=1,ncols=5)
axs = gs.subplots(sharex=True, sharey=True)
# fig.suptitle('Under Sampling Variations')

label_count_no_sampling = pd.DataFrame(y_train).value_counts()
label_count_no_sampling.plot(kind='bar',ax = axs[0])
axs[0].set_title('No sampling')

# Hide x labels and tick labels for all but bottom plot.
for ax in axs:
    x_axis = ax.axes.get_xaxis()
    x_axis.set_visible(False)
    ax.label_outer()
cnt = 1

for var,my_dict in var_to_dict_under.items():
    samplers['NearMiss_' + var] = NearMiss(
                                        sampling_strategy=my_dict,
                                        version=2,
                                        # size_ngh=None,
                                        n_neighbors=5,
                                        # ver3_samp_ngh=None,
                                        # n_neighbors_ver3=3,
                                        # n_jobs=1
                                        )

    # this is inefficient but its ok... we can live with it:
    # X_sampled, y_sampled = samplers['borderlineSMOTE_' + var].fit_resample(X_train, y_train)
    X_sampled, y_sampled = samplers['NearMiss_' + var].fit_resample(X_train, y_train)
    label_count_oversampled = pd.DataFrame(y_sampled).value_counts()
    axs[cnt].set_title(var)
    label_count_oversampled.plot(kind='bar', ax=axs[cnt])
    cnt += 1

best_smote_acc = 0
best_borderline_smote_acc = 0
best_nearmiss_acc = 0

nearmiss_to_acc = {}
nearmiss_to_f1macro = {}
nearmiss_to_f1weigthed  = {}
smote_to_acc = {}
smote_to_f1macro = {}
smote_to_f1weigthed  = {}
bordeline_smote_to_acc = {}
bordeline_smote_to_f1macro = {}
bordeline_smote_to_f1weigthed = {}

key_to_accuracy = {}
for str,sampler in samplers.items():
    if sampler == None:
        X_sampled, y_sampled = X_train, y_train
    else:
        X_sampled, y_sampled = sampler.fit_resample(X_train, y_train)

    model = MLPClassifier(random_state=1, max_iter=100 )
    # model = DecisionTreeClassifier()

    model.fit(X_sampled, y_sampled)
    y_pred = model.predict(X_test)
    # precision = precision_score(y_test, y_pred,average="macro")
    # recall = recall_score(y_test, y_pred,average="macro")
    accuracy = balanced_accuracy_score(y_test, y_pred)
    f1w = f1_score(y_test, y_pred,average="weighted")

    # cmap = 'PuRd'
    # cm = confusion_matrix(encoder.inverse_transform(y_test), encoder.inverse_transform(y_pred))
    # plot_confusion_matrix_from_data(encoder.inverse_transform(y_test), encoder.inverse_transform(y_pred), columns=columns,title_str = str)
    #
    print(str+" --> ",accuracy)
    # print('----------------------')


    if "NearMiss" in str:
        nearmiss_to_acc[str.split('_')[-1]] = float(accuracy)
        nearmiss_to_f1weigthed[str.split('_')[-1]] = float(f1w)
        if accuracy+f1w>best_nearmiss_acc:
            best_nearmiss_acc = accuracy+f1w
            best_nearmiss = model
            best_nearmiss_str = str
    elif "borderline" in str:
        bordeline_smote_to_acc[str.split('_')[-1]] = float(accuracy)
        bordeline_smote_to_f1weigthed[str.split('_')[-1]] = float(f1w)
        if  accuracy+f1w>best_borderline_smote_acc:
            best_borderline_smote_acc = accuracy+f1w
            best_borderline_smote = model
            best_borderline_smote_str = str
    elif "SMOTE" in str:
        smote_to_acc[str.split('_')[-1]] = float(accuracy)
        smote_to_f1weigthed[str.split('_')[-1]] = float(f1w)
        if accuracy+f1w>best_smote_acc:
            best_smote_acc = accuracy+f1w
            best_smote = model
            best_smote_str = str
    elif str == 'None':
        nearmiss_to_acc['None'] = float(accuracy)
        nearmiss_to_f1weigthed['None'] = float(f1w)

        bordeline_smote_to_acc['None'] = float(accuracy)
        bordeline_smote_to_f1weigthed['None'] = float(f1w)

        smote_to_acc['None'] = float(accuracy)
        smote_to_f1weigthed['None'] = float(f1w)

        no_sampling = model


common_funcs.bar_plot_metrics(nearmiss_to_acc,nearmiss_to_f1weigthed, ['None']+under_variations)

common_funcs.bar_plot_metrics(bordeline_smote_to_acc,bordeline_smote_to_f1weigthed,['None']+over_variations)
plt.show()

common_funcs.bar_plot_metrics(smote_to_acc,smote_to_f1weigthed,['None']+over_variations)

for str,model in zip([best_nearmiss_str,best_borderline_smote_str,best_smote_str,'None'] , [best_nearmiss,best_borderline_smote,best_smote,no_sampling]):
    y_pred = model.predict(X_test)
    precision = precision_score(y_test, y_pred,average="macro")
    recall = recall_score(y_test, y_pred,average="macro")
    accuracy = balanced_accuracy_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred,average="macro")

    print(str + " -->")
    # print('auc score:',auc_score1)
    print('balanced accuracy:', accuracy)
    print('precision', precision)
    print('recall', recall)
    print('f1:', f1)
    print('----------------------')
    plot_confusion_matrix_from_data(encoder.inverse_transform(y_test), encoder.inverse_transform(y_pred), columns=columns,title_str = str)

