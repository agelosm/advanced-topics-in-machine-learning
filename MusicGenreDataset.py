# =============================================================================
# Advanced Topics in Machine Learning: Project on Class Imbalance/Active Learning
# Charilaos Bezdemiotis, mpezdemc@csd.auth.g, AM: 77, Spring Semester 2021
# Agelos Mourelis, amourelis@csd.auth.gr, AM: 61, Spring Semester 2021
# =============================================================================
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler


class MusicGenreDataset:

    def __init__(self, path, make_binary=False):
        self.path = path
        self.make_binary = make_binary

    def read_csv(self):
        df = pd.read_csv(self.path)

        if self.make_binary:
            self.genre_df = self.make_binarization(key='folk')
        else:
            self.genre_df = df.iloc[:, 0]

        # drop columns that do not provide knowledge
        df = df.drop(labels='genre', axis=1)
        df = df.drop(labels='track_id', axis=1)
        df = df.drop(labels='artist_name', axis=1)
        df = df.drop(labels='title', axis=1)

        self.df = df

        self.encoder = LabelEncoder()
        self.yy = self.encoder.fit_transform(self.genre_df)

        self.y = np.zeros_like(self.yy)
        for i,yyy in enumerate(self.yy):
            if yyy == 0:
                self.y[i] = 0
            else:
                self.y[i] = 1

        self.key_to_original_label, self.columns = self.decode()

        self.train_len = len(self.y) * 0.66

    def preprocess_data(self):
        # normalize the data
        self.scaler = StandardScaler()
        self.X_train = self.scaler.fit_transform(self.X_train)
        self.X_test = self.scaler.transform(self.X_test)

    def get_train_test_split(self):
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.df.iloc[:, :], self.y,
                                                                                test_size=0.33, random_state=42)
        self.preprocess_data()
        return self.X_train, self.X_test, self.y_train, self.y_test

    def plot_class_distribution(self):
        self.label_count = self.df.iloc[:, 0].value_counts()
        self.plot_distribution(self.label_count, kind='pie', title='Origin Dataset')

    def plot_train_dataset(self):
        self.plot_distribution(pd.DataFrame(self.encoder.inverse_transform(self.y_train)).value_counts(),
                                       kind='pie', title='Train Dataset')

    def plot_test_dataset(self):
        self.plot_distribution(pd.DataFrame(self.encoder.inverse_transform(self.y_test)).value_counts(),
                                       kind='pie', title='Test Dataset')

    def plot_distribution(self, input_df, kind, title=None):
        plt.figure()
        if title is not None:
            plt.title(title)
        input_df.plot(kind=kind)
        plt.minorticks_on()
        plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
        plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
        return 1

    # Function that turns the multi-class problem to binary, with a one-against-all for 'key' column
    def make_binarization(self, key, plot=False):
        genre_list = []
        cnt = 0
        for item in self.df.iloc[:, 0]:
            if item == key:
                genre_list.append(key)
            else:
                genre_list.append('not' + key)
        genre_df = pd.DataFrame(genre_list)

        # PLOT class distribution after binarization
        if plot:
            label_count = genre_df.value_counts()
            self.plot_distribution(label_count, kind='pie')
            self.plot_distribution(label_count, kind='pie')

        return genre_df

    # Decode the labels, from their encoding, for visualization purposes
    def decode(self):
        labels_set = set(self.y)
        key_to_original_label = dict()
        i = 0
        continue_looping = True
        while continue_looping:
            if self.y[i] in labels_set:
                key_to_original_label[self.y[i]] = self.genre_df[i]
                labels_set.remove(self.y[i])
            if labels_set == set():
                continue_looping = False
            i += 1

        columns = []
        for i in range(len(set(self.y))):
            columns.append(key_to_original_label[i])

        return key_to_original_label, columns