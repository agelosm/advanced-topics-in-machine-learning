# =============================================================================
# Advanced Topics in Machine Learning: Project on Class Imbalance/Active Learning
# Charilaos Bezdemiotis, mpezdemc@csd.auth.g, AM: 77, Spring Semester 2021
# Agelos Mourelis, amourelis@csd.auth.gr, AM: 61, Spring Semester 2021
# =============================================================================

from datetime import datetime
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Code to print graph bars from some timesteps taken during initialization and evaluation on Google Colab

start_init_uncertainty = datetime.strptime('20:37:04.892086', "%H:%M:%S.%f")
start_init_committee = datetime.strptime('20:37:04.965404', "%H:%M:%S.%f")
start_init_density = datetime.strptime('20:37:05.077412', "%H:%M:%S.%f")

start_unc_calc = datetime.strptime('20:48:38.846466', "%H:%M:%S.%f")
start_comm_calc = datetime.strptime('20:54:04.180972', "%H:%M:%S.%f")
start_denc_calc = datetime.strptime('21:47:18.490366', "%H:%M:%S.%f")
start_random_calc = datetime.strptime('21:59:16.485486', "%H:%M:%S.%f")
end_random_calc = datetime.strptime('22:04:28.644635', "%H:%M:%S.%f")

init_times = [pd.Timedelta(start_init_committee-start_init_uncertainty), pd.Timedelta(start_init_density-start_init_committee),
              pd.Timedelta(start_unc_calc-start_init_density)]

calc_times = [pd.Timedelta(start_comm_calc-start_unc_calc), pd.Timedelta(start_denc_calc-start_comm_calc),pd.Timedelta(start_random_calc-start_denc_calc),
              pd.Timedelta(end_random_calc-start_random_calc)]

print(init_times)
print(calc_times)

font = {'family' : 'normal',
        'size'   : 22}

matplotlib.rc('font', **font)

df = pd.DataFrame( {'AL Method':['Uncertainty', 'Committee', 'Density','Random'], 'val':calc_times})
df['val'] = df['val'] / np.timedelta64(1, 'm')

df.plot.bar(x='AL Method', y='val', rot=0,xlabel='', ylabel='Time(min)', legend=False, color=plt.cm.Paired(np.arange(len(df))))


df = pd.DataFrame( {'AL Method':['Uncertainty', 'Committee', 'Density'], 'val':init_times})
df['val'] = df['val'] / np.timedelta64(1, 'm')

df.plot.bar(x='AL Method', y='val', rot=0, xlabel='', ylabel='Log Time(min)', legend=False, logy=True, color=plt.cm.Paired(np.arange(len(df))))
plt.show()